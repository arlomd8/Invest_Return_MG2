﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Threading;

public class Money1 : MonoBehaviour
{
    public int money;
    public int retur;
    public int invest;
    public Text moneyText;
    public Text investText;
    public Text returnText;
    void Start()
    {
        money = 100;
        SetCountText();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LH"))
        {
            invest = 5;
            retur = 4 * 5;
            money -= invest;
            SetCountText();
            SetInvestText();
            StartCoroutine("ReturnValSlow");

        }

        if (other.gameObject.CompareTag("LL"))
        {
            invest = 5;
            retur = 2 * 5;
            money -= 5;
            SetCountText();
            SetInvestText();
            StartCoroutine("ReturnVal");

        }

        if (other.gameObject.CompareTag("LI"))
        {
            invest = 10;
            retur = 5;
            money -= invest;
            SetCountText();
            SetInvestText();
            StartCoroutine("ReturnVal");

        }
        if (other.gameObject.CompareTag("HH"))
        {
            invest = 50;
            retur = 2 * 50;
            money -= invest;
            SetCountText();
            SetInvestText();
            StartCoroutine("ReturnValSlow");

        }
        if (other.gameObject.CompareTag("HL"))
        {
            invest = 60;
            retur = 75;
            money -= invest;
            SetCountText();
            SetInvestText();
            StartCoroutine("ReturnVal");
        }
        if (other.gameObject.CompareTag("HI"))
        {
        invest = 50;
        retur = 10;
        money -= invest;
        SetCountText();
        SetInvestText();
        StartCoroutine("ReturnVal");

        }
        

    }

    private IEnumerator ReturnVal()
    {
        money += retur;
        yield return new WaitForSeconds(5);
        SetCountText();
        SetReturnText();

    }

    private IEnumerator ReturnValSlow()
    {
        money += retur;
        yield return new WaitForSeconds(10);
        SetCountText();
        SetReturnText();

    }

    public void SetCountText()
    {
        moneyText.text = "Money : " + money.ToString();
    }
    public void SetInvestText()
    {
        investText.text = "Invest : " + invest.ToString();
    }
    public void SetReturnText()
    {
        returnText.text = "Return : " + retur.ToString();
       
    }
  
    void Update()
    {

    }
}
