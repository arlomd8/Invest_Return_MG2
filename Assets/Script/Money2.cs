﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Money2 : MonoBehaviour
{
    public int money2;
    public Text moneyText2;
    // Start is called before the first frame update
    void Start()
    {
        money2 = 100;
        SetCountText2();
    }
    public void SetCountText2()
    {
       moneyText2.text = "Money : " + money2.ToString();
    }
    // Update is called once per frame
    void Update()
    {
       
    }
}
